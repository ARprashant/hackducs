from flask import Flask,jsonify,Response
import pandas as pd
import json
from goodreads import client
import re
from collections import defaultdict
from bs4 import BeautifulSoup as bs
gc = client.GoodreadsClient("I8RMWKdI2Mt36VkvbmhvQ","yA9Jemwi5HhIeCtbqmkAQ7GDViPsTc9IFa7lBQQ5o")

app = Flask(__name__)
app.config['SECRET_KEY'] = '98f3cc2d25173603ce0ecbcbb60956d263d00189f0a1342907f1527a41870c2d'

data = pd.read_csv("train_user_books.csv")
book_df = pd.read_csv("complete_data.csv")
ratings = pd.read_csv('BX-Book-Ratings.csv', sep=';', error_bad_lines=False, encoding="latin-1")
books_rated_by_users = ratings.merge(book_df,on="ISBN")
print(books_rated_by_users.columns)

def get_description(x):
  try:
    book = gc.book(isbn = str(x))
    ratings = str(book.rating_dist)
    # rating_0 =
    details = {
        "rating_dist" : str(book.rating_dist),
        "reviews" : str(book.text_reviews_count),
        "description": bs(book.description).text,
        "pages":str(book.num_pages),
        "genres": set(["fiction","romance","business","horror","biography","fantasy", "poetry"]).intersection(set([str(i).lower() for i  in book.popular_shelves])),
        "format":str(book.format)
    }
    return details

  except Exception as e:
    print(e)

def alter(data):
	js = defaultdict(list)
	for i,row in data.iterrows():
		js[row.Value].append({
    "ISBN": row['ISBN'],
    "Title": row['bookTitle'],
    "bookAuthor": row['bookAuthor'],
    "published_in": row["yearOfPublication"],
    "Publisher": row['publisher'],
    "image_s": row['imageUrlS'],
    "image_m": row['imageUrlM'],
    "image_l": row['imageUrlL'],
    "Reviews":row["reviews"],
    "Description":bs(row["description"]).text,
    "Pages":row["pages"],
    "Format":row["format"],
    "Rating_5":row["rating_5"],
    "Rating_4":row["rating_4"],
    "Rating_3":row["rating_3"],
    "Rating_2":row["rating_2"],
    "Rating_1":row["rating_1"],
    "Rating_Total":row["rating_total_count"]
    }
    )

	return js 

@app.route("/")
def homepage():
	popular = pd.concat([pd.read_csv("homepage_data.csv"),pd.read_csv("Popular_books.csv").head(20)])
	js = alter(popular)
	response = jsonify(js)
	response.headers.add('Access-Control-Allow-Origin', '*')
	return response

@app.route("/dataset")
def dataset():
	m = book_df.drop_duplicates("ISBN").head(200)
	m.description = m.description.apply(lambda x:bs(x).text)
	m = m.rename(columns ={
	    'ISBN':"ISBN",
	    'bookTitle':"Title" ,
	    'bookAuthor':"bookAuthor" ,
	    "yearOfPublication":"published_in",
	    'publisher':"Publisher",
	    'imageUrlS':"image_s",
	    'imageUrlM':"image_m",
	    'imageUrlL':"image_l",
	    "reviews":"Reviews",
	    "description":"Description",
	    "pages":"Pages",
	    "format":"Format",
	    "rating_5":"Rating_5",
	    "rating_4":"Rating_4",
	    "rating_3":"Rating_3",
	    "rating_2":"Rating_2",
	    "rating_1":"Rating_1",
	    "rating_total_count":"Rating_Total",
    })
	return m.to_json(orient='records')

@app.route("/books/<string:value>")
def view_value(value):
	if value.lower() == "popular":
		d = book_df.sort_values("rating_total_count",ascending=True).drop_duplicates("bookTitle").head(25)
	elif value.lower() in ["romance","biography","fiction"]:
		d = book_df[book_df.Value == value].sort_values("rating_total_count",ascending=True).drop_duplicates("bookTitle").head(25)
	d.description = d.description.apply(lambda x:bs(x).text)
	d = d.rename(columns ={
	    'ISBN':"ISBN",
	    'bookTitle':"Title" ,
	    'bookAuthor':"bookAuthor" ,
	    "yearOfPublication":"published_in",
	    'publisher':"Publisher",
	    'imageUrlS':"image_s",
	    'imageUrlM':"image_m",
	    'imageUrlL':"image_l",
	    "reviews":"Reviews",
	    "description":"Description",
	    "pages":"Pages",
	    "format":"Format",
	    "rating_5":"Rating_5",
	    "rating_4":"Rating_4",
	    "rating_3":"Rating_3",
	    "rating_2":"Rating_2",
	    "rating_1":"Rating_1",
	    "rating_total_count":"Rating_Total",
    })
	return d.to_json(orient="records")

@app.route("/recommend/user/<int:user_id>",methods=["Get","POST"])
def test(user_id):
	temp = data[data['User-ID'] == user_id].merge(book_df)
	temp = temp.reset_index().drop(["index","User-ID"],axis=1).reset_index()
	temp = temp.drop_duplicates("Book-Title")
	temp.description = temp.description.apply(lambda x:bs(x).text)
	temp=temp.rename(columns ={
	    'ISBN':"ISBN",
	    'bookTitle':"Title" ,
	    'bookAuthor':"bookAuthor" ,
	    "yearOfPublication":"published_in",
	    'publisher':"Publisher",
	    'imageUrlS':"image_s",
	    'imageUrlM':"image_m",
	    'imageUrlL':"image_l",
	    "reviews":"Reviews",
	    "description":"Description",
	    "pages":"Pages",
	    "format":"Format",
	    "rating_5":"Rating_5",
	    "rating_4":"Rating_4",
	    "rating_3":"Rating_3",
	    "rating_2":"Rating_2",
	    "rating_1":"Rating_1",
	    "rating_total_count":"Rating_Total"
    })
	# temp["rating__"] = temp['Rating'].apply(lambda x:float("%.6f" % float(x)))
	# .sort_values("rating__",ascending=False).
	return temp.to_json(orient="records")

@app.route("/api/test/books/<int:count>",methods=["Get","POST"])
def c_books(count):
	temp = book_df.iloc[:int(count)]
	temp = temp.drop_duplicates("Book-Title")
	temp = temp.rename(columns = {"index":"id",
	                  "Book-Title":"Title",
	                  "Book-Rating":"Rating",
	                  "Book-Author":"Author",
	                  "Year-Of-Publication":"published_in",
	                  "Image-URL-S" :"image_s",
	                  "Image-URL-M" :"image_m",
	                  "Image-URL-L" :"image_l",
	                  })
	
	return temp.to_json(orient="records")

@app.route("/api/test/user/<int:user_id>",methods=["Get","POST"])
def book_read_by_user(user_id):
	temp = books_rated_by_users[books_rated_by_users["User-ID"] == user_id]
	temp = temp.drop_duplicates("Book-Title")
	temp = temp.rename(columns = {"index":"id",
	                  "Book-Title":"Title",
	                  "Book-Rating":"Rating",
	                  "Book-Author":"Author",
	                  "Year-Of-Publication":"published_in",
	                  "Image-URL-S" :"image_s",
	                  "Image-URL-M" :"image_m",
	                  "Image-URL-L" :"image_l",
	                  })
	
	# recommended = test(user_id)
	# temp = pd.concat([temp.iloc[:15],recommended])
	return temp.to_json(orient="records")

@app.route("/book/isbn/<string:isbn>",methods=["Get","POST"])
def book_by_isbn(isbn):
	d = book_df[book_df.ISBN == isbn].head(1)
	d.description = d.description.apply(lambda x:bs(x).text)
	d = d.rename(columns ={
	    'ISBN':"ISBN",
	    'bookTitle':"Title" ,
	    'bookAuthor':"bookAuthor" ,
	    "yearOfPublication":"published_in",
	    'publisher':"Publisher",
	    'imageUrlS':"image_s",
	    'imageUrlM':"image_m",
	    'imageUrlL':"image_l",
	    "reviews":"Reviews",
	    "description":"Description",
	    "pages":"Pages",
	    "format":"Format",
	    "rating_5":"Rating_5",
	    "rating_4":"Rating_4",
	    "rating_3":"Rating_3",
	    "rating_2":"Rating_2",
	    "rating_1":"Rating_1",
	    "rating_total_count":"Rating_Total",
    })
	return d.to_json(orient="records")

if __name__ == "__main__":
        app.run(host="192.168.43.189",debug=True)
